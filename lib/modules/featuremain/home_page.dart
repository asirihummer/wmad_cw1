import 'package:flutter/material.dart';
import 'package:wmad_cw1_contact_buddy/modules/featuremain/db_helper.dart';

class HomePage extends StatefulWidget {
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<Map<String, dynamic>> _dataSet = [];
  bool _isLoading = true;

  void _refreshDataSet() async {
    final dataSet = await HelperSql.getAllContacts();
    setState(() {
      _dataSet = dataSet;
      _isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    _refreshDataSet();
  }

  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _phoneController = TextEditingController();

  Future<void> _searchContactsByName(String searchstring) async {
    final dataSet = await HelperSql.getContactByName(searchstring);
    setState(() {
      _dataSet = dataSet;
      _isLoading = false;
    });
  }

  Future<void> _addContacts() async {
    await HelperSql.createContacts(_nameController.text, _phoneController.text);
    _refreshDataSet();
  }

  Future<void> _updateContacts(int id) async {
    await HelperSql.updateContacts(
        id, _nameController.text, _phoneController.text);
    ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text("Contact Successfully Updated")));
    _refreshDataSet();
  }

  Future<void> _deleteContact(int id) async {
    await HelperSql.deleteContact(id);
    ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text("Contact Successfully Deleted")));
    _refreshDataSet();
  }

  void showBottomSheet(int? id) async {
    if (id != null) {
      final oldContacts = _dataSet.firstWhere((element) => element['id'] == id);
      _nameController.text = oldContacts['name'];
      _phoneController.text = oldContacts['phone'];
    }

    showModalBottomSheet(
      context: context,
      elevation: 5,
      isScrollControlled: true,
      builder: (_) => Container(
        padding: EdgeInsets.only(
            top: 30,
            right: 15,
            bottom: MediaQuery.of(context).viewInsets.bottom + 50,
            left: 15),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            TextField(
              controller: _nameController,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: "Name",
              ),
            ),
            SizedBox(height: 15),
            TextField(
              controller: _phoneController,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: "Phone Number",
              ),
            ),
            SizedBox(height: 15),
            ElevatedButton(
              onPressed: () async {
                if (id == null) {
                  await _addContacts();
                }
                if (id != null) {
                  await _updateContacts(id);
                }

                _nameController.text = "";
                _phoneController.text = "";

                Navigator.of(context).pop();
              },
              child: Padding(
                padding: EdgeInsets.all(15),
                child: Text(id == null ? "Add New Contact" : "Update"),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFD1C4E9),
      appBar: AppBar(
        title: Text("Contacts Buddy"),
      ),
      body: _isLoading
          ? Center(
        child: CircularProgressIndicator(),
      )
          : Column(
        children: [
          Padding(
            padding: EdgeInsets.all(10),
            child: TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Search Contacts',
              ),
              onChanged: (value) async {
                print(value);
                await _searchContactsByName(value);
              },
            ),
          ),
          Expanded(
            child: ListView.builder(
              itemCount: _dataSet.length,
              itemBuilder: (context, index) => Card(
                margin: EdgeInsets.all(15),
                child: ListTile(
                  title: Padding(
                    padding: EdgeInsets.symmetric(vertical: 5),
                    child: Text(
                      _dataSet[index]['name'],
                    ),
                  ),
                  subtitle: Padding(
                    padding: EdgeInsets.symmetric(vertical: 5),
                    child: Text(
                      _dataSet[index]['phone'],
                    ),
                  ),
                  trailing: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      IconButton(
                        onPressed: () {
                          showBottomSheet(_dataSet[index]['id']);
                        },
                        icon: Icon(Icons.edit),
                      ),
                      IconButton(
                        onPressed: () {
                          _deleteContact(_dataSet[index]['id']);
                        },
                        icon: Icon(Icons.delete),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => showBottomSheet(null),
        child: Icon(Icons.add),
      ),
    );
  }
}
