import 'package:sqflite/sqflite.dart' as sql;

class HelperSql {
  static Future<void> createTables(sql.Database database) async{
    await database.execute("CREATE	TABLE	contacts(id	INTEGER	PRIMARY	KEY	AUTOINCREMENT,	name	TEXT	NOT	NULL,phone	TEXT	NOT	NULL)");
  }

  static Future<sql.Database> db() async{
    return sql.openDatabase(
      "database_name.db",
      version: 1,
      onCreate: (sql.Database database, int version) async{
        await createTables(database);
      });
  }

  static Future<int> createContacts(String name, String phone) async{
    final db = await HelperSql.db();
    final contacts = {'name':name,'phone':phone};
    final id = await db.insert('contacts', contacts,conflictAlgorithm: sql.ConflictAlgorithm.replace);
    return id;
  }

  static Future<List<Map<String, dynamic>>> getAllContacts() async{
    final db = await HelperSql.db();
    return db.query('contacts',orderBy: 'id');
  }

  static Future<List<Map<String, dynamic>>> getContact(int id) async{
    final db = await HelperSql.db();
    return db.query('contacts',where: "id = ?", whereArgs: [id], limit: 1);
  }

  static Future<List<Map<String, dynamic>>> getContactByName(String name) async{
    final db = await HelperSql.db();
    if(name==''){
      return db.query('contacts',orderBy: 'id');
    }
    final result = await db.query('contacts',where: "name = ?", whereArgs: [name]);
    return result;
  }

  static Future<int> updateContacts(int id, String name, String phone) async{
    final db = await HelperSql.db();
    final contacts = {'name':name,'phone':phone};
    final result = await db.update('contacts', contacts, where: "id = ?", whereArgs: [id]);
    return result;
  }

  static Future<void> deleteContact(int id) async{
    final db = await HelperSql.db();
    try{
      await db.delete('contacts',where: "id = ?", whereArgs: [id]);
    }catch(e){}
  }
}

